package main

import (
	// "time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

//InitRouter is used for initiate router
func InitRouter() *gin.Engine {
	router := gin.Default()
	router.Use(cors.Default())

	//routers
	router.GET("/", GetAllData)
	return router
}
