package main

import (
	// "fmt"
	"net/http"
	"github.com/gin-gonic/gin"
)

func GetAllData(c *gin.Context) {

	message, err := getAllData()
	if err == nil {
		if len(message) == 0{
			c.JSON(http.StatusOK, gin.H{"Status": http.StatusOK, "data": "Data Kosong"})
		}else{
			c.JSON(http.StatusOK, gin.H{"Status": http.StatusOK, "data": message})
		}	
	} else {
		c.JSON(http.StatusOK, gin.H{"Status": "Error", "Error Message": err.Error()})
	}
}