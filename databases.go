package main

import (
	"fmt"
	// "os"

	"github.com/jinzhu/gorm"

	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func connect() (*gorm.DB, error) {
	Db, err := gorm.Open("mysql", "root:@(localhost)/customer_info?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	
	if err != nil {
		fmt.Println("Connection Database Error ", err.Error())
	} else {
		Db.DB().SetMaxIdleConns(2)
		Db.DB().SetMaxOpenConns(1000)
		// Db.DB().SetConnMaxLifetime(10)
		Db.LogMode(true)
		fmt.Println(Db)
		fmt.Println("Connected")
	}

	return Db, err
}