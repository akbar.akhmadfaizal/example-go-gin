package main

type CustomerInfoResponse struct{
	CIFNO					string `json:"cifno" gorm:"column:cifno"`
	NAMA_SESUAI_ID			string `json:"nama_sesuai_id" gorm:"column:nama_sesuai_id"`
	TIPE_NASABAH			string `json:"tipe_nasabah" gorm:"column:tipe_nasabah"`
	TIPE_NASABAH_DESC		string `json:"tipe_nasabah_desc" gorm:"column:tipe_nasabah_desc"`
	JENIS_KELAMIN			string `json:"jenis_kelamin" gorm:"column:jenis_kelamin"`
	KEWARGANEGARAAN			string `json:"kewarganegaraan" gorm:"column:kewarganegaraan"`
	KODE_PENDIDIKAN			string `json:"kode_pendidikan" gorm:"column:kode_pendidikan"`
	PENDIDIKAN				string `json:"pendidikan" gorm:"column:pendidikan"`
	KODE_JENIS_PEKERJAAN	string `json:"kode_jenis_pekerjaan" gorm:"column:kode_jenis_pekerjaan"`
	JENIS_PEKERJAAN			string `json:"jenis_pekerjaan" gorm:"column:jenis_pekerjaan"`
	NAMA_KANTOR				string `json:"nama_kantor" gorm:"column:nama_kantor"`
	KODE_BIDANG_PEKERJAAN	string `json:"kode_bidang_pekerjaan" gorm:"column:kode_bidang_pekerjaan"`
	BIDANG_PEKERJAAN		string `json:"bidang_pekerjaan" gorm:"column:bidang_pekerjaan"`
	KODE_JABATAN			string `json:"kode_jabatan" gorm:"column:kode_jabatan"`
	JABATAN					string `json:"jabatan" gorm:"column:jabatan"`
}