-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 29, 2020 at 05:51 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `customer_info`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer_info`
--

CREATE TABLE `customer_info` (
  `cifno` varchar(100) NOT NULL,
  `nama_sesuai_id` text NOT NULL,
  `tipe_nasabah` text NOT NULL,
  `tipe_nasabah_desc` text NOT NULL,
  `jenis_kelamin` text NOT NULL,
  `kewarganegaraan` text NOT NULL,
  `kode_pendidikan` text NOT NULL,
  `pendidikan` text NOT NULL,
  `kode_jenis_pekerjaan` text NOT NULL,
  `jenis_pekerjaan` text NOT NULL,
  `nama_kantor` text NOT NULL,
  `kode_bidang_pekerjaan` text NOT NULL,
  `bidang_pekerjaan` text NOT NULL,
  `kode_jabatan` text NOT NULL,
  `jabatan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer_info`
--

INSERT INTO `customer_info` (`cifno`, `nama_sesuai_id`, `tipe_nasabah`, `tipe_nasabah_desc`, `jenis_kelamin`, `kewarganegaraan`, `kode_pendidikan`, `pendidikan`, `kode_jenis_pekerjaan`, `jenis_pekerjaan`, `nama_kantor`, `kode_bidang_pekerjaan`, `bidang_pekerjaan`, `kode_jabatan`, `jabatan`) VALUES
('B041234', 'BS HARJONO', 'I', 'Individual                              ', 'M', 'Indonesia           ', '', '', '', '', '', '', '', '', ''),
('B041235', 'BUDI RAHAYU SAPTAWATI', 'I', 'Individual                              ', 'F', 'Indonesia           ', 'S1', 'Sarjana             ', 'PENS ', 'Pensiunan                               ', 'KLATEN                                  ', '9999', 'PENSIUNAN                               ', '61', 'Pengurus, Lainnya                       '),
('B041236', 'BUN ANAM', 'I', 'Individual                              ', 'M', 'Indonesia           ', '', '', 'MILD ', 'Militer (TNI - AD)                      ', 'KODIM KLATEN                            ', '1011', 'Pertanian                               ', '61', 'Pengurus, Lainnya                       '),
('B041237', 'BAMBANG SUGENG', 'I', 'Individual                              ', 'M', 'Indonesia           ', '', '', '     ', '', 'BAMBANG SUGENG                          ', '    ', '', '     ', ''),
('B041238', 'BEJO JINTO PRAYITNO', 'I', 'Individual                              ', 'M', 'Indonesia           ', 'SM', 'SMP                 ', 'WIRA ', 'Wiraswasta                              ', 'KONVEKSI FITROH                         ', '1012', 'Bahan Mentah                            ', '61', 'Pengurus, Lainnya                       '),
('B041239', 'BAMBANG SUWANDI', 'I', 'Individual                              ', 'M', 'Indonesia           ', '', '', '', '', '', '', '', '', ''),
('B041240', 'BAGUS PRIBADI/BANGUN AJI MARTANI', 'I', 'Individual                              ', 'M', 'Indonesia           ', '', '', '', '', '', '', '', '', ''),
('B041241', 'BAMBANG MARGIYANTO', 'I', 'Individual                              ', 'M', 'Indonesia           ', 'SU', 'SMU/SMK             ', 'WIRA ', 'Wiraswasta                              ', 'DAGANG                                  ', '1011', 'Pertanian                               ', '61', 'Pengurus, Lainnya                       '),
('B041242', 'BASUKI HARJA SUPARTA B', 'I', 'Individual                              ', 'M', 'Indonesia           ', '', '', '', '', '', '', '', '', ''),
('B041243', 'SETAWAN RAHARJO B', 'I', 'Individual', 'M', 'Indonesia', 'S1', 'Sarjana', '', '', '', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer_info`
--
ALTER TABLE `customer_info`
  ADD PRIMARY KEY (`cifno`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
