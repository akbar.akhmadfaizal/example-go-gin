package main

import (
	"fmt"
)


func getAllData() ([]CustomerInfoResponse, error) {
	var customer_info []CustomerInfoResponse = []CustomerInfoResponse{}

	//Create Connection with Phoenix
	var db, err = connect()

	if err != nil {
		fmt.Println(err.Error())
		return customer_info, err
	}

	err = db.
		Table("customer_info").
		Scan(&customer_info).Error

	if err != nil {
		fmt.Println(err.Error())
		return customer_info, err
	}

	return customer_info, nil
}