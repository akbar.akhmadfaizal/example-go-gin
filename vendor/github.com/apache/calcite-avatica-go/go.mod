module github.com/apache/calcite-avatica-go/v3

require (
	github.com/apache/calcite-avatica-go v0.0.0-20180828061053-334bc15f92dd
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/protobuf v1.2.0
	github.com/hashicorp/go-uuid v0.0.0-20180228145832-27454136f036
	github.com/jcmturner/gofork v0.0.0-20180107083740-2aebee971930 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	github.com/xinsnake/go-http-digest-auth-client v0.4.0
	golang.org/x/crypto v0.0.0-20180820150726-614d502a4dac // indirect
	golang.org/x/net v0.0.0-20180826012351-8a410e7b638d
	golang.org/x/sync v0.0.0-20180314180146-1d60e4601c6f // indirect
	gopkg.in/jcmturner/aescts.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/dnsutils.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/goidentity.v2 v2.0.0 // indirect
	gopkg.in/jcmturner/gokrb5.v5 v5.3.0
	gopkg.in/jcmturner/rpc.v0 v0.0.2 // indirect
)
